#!/bin/bash

# Define the map of replacements
declare -A replacements
replacements=(
  ["type: all_languages_entity_reference"]="type: informea_api_all_languages_entity_reference"
  ["type: all_languages_string"]="type: informea_api_all_languages_string"
  ["type: all_languages_summary"]="type: informea_api_all_languages_summary"
  ["type: boolean_formatter"]="type: informea_api_boolean"
  ["type: decision_number"]="type: informea_api_decision_number"
  ["type: decision_published"]="type: informea_api_decision_published"
  ["type: informea_country_iso3"]="type: informea_api_country_iso3"
  ["type: informea_files"]="type: informea_api_files"
  ["type: informea_power_tags"]="type: informea_api_power_tags"
  ["type: informea_term_entity_reference"]="type: informea_api_term_entity_reference"
  ["type: informea_timestamp"]="type: informea_api_timestamp"
  ["type: legislation_relations"]="type: informea_api_legislation_relations"
  ["type: informea_link_with_text"]="type: informea_api_link_with_text"
  ["type: meeting_image_copyright"]="type: informea_api_meeting_image_copyright"
  ["type: meeting_status"]="type: informea_api_meeting_status"
  ["type: meeting_type"]="type: informea_api_meeting_type"
  ["type: meeting_virtual"]="type: informea_api_meeting_virtual"
  ["type: null_formatter"]="type: informea_api_null_formatter"
  ["type: string_to_boolean_formatter"]="type: informea_api_string_to_boolean"
  ["type: treaties"]="type: informea_api_treaties"
  ["type: treaty"]="type: informea_api_treaty"
  ["type: visibility"]="type: informea_api_visibility"
  ["type: multiple_value_list_text"]="type: informea_api_multiple_value_list_text"
)

# Get the file name
file=$1

# Get the treaty
treaty=$2

# Create a temporary file to store the modified contents
temp_file=$(mktemp)

# Iterate over each line in the file
while IFS= read -r line; do
  # Replace each string in the line with its corresponding replacement
  for key in "${!replacements[@]}"; do
    line=${line//$key/${replacements[$key]}}
  done

  if [[ $line == *"type: informea_api_treaty"* ]]; then
    printf "%s\n" "          settings:" >> "$temp_file"
    printf "%s\n" "            treaties: $treaty" >> "$temp_file"
  fi

  if [[ $line == *"type: informea_api_treaties"* ]]; then
    echo "          settings:" >> "$temp_file"
    echo "            treaties: $treaty" >> "$temp_file"
  fi

  # Write the modified line to the temporary file
  printf "%s\n" "$line" >> "$temp_file"
done < "$file"

# Overwrite the original file with the contents of the temporary file
mv "$temp_file" "$file"
