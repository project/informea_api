# InforMEA API module

This module contains of list of tools that can be used to create API endpoints that can be used by InforMEA to import data.

This process involves creating a view with the "InforMEA serializer" format plugin for each content type you want to expose. Additionally, when adding fields to the view, use the serializable versions of the fields (e.g. Title (serializable)).

## Supported content types:

- meetings
- decisions
- contacts
- national reports
- action plans

## API Fields

### Generic
- nid - node ID field.
- id - node UUID field
- url/link - link to content field, make sure to make the link absolute
- title - node title, with the *\[InforMEA\] String in all languages* formatter
- summary - node body, with the *\[InforMEA\] Summary in all languages* formatter
- type - usually *Key* formatter value for List (text) fields
- treaties - *\[InforMEA\] Treaties* formatter on the Node ID
- treaty - *\[InforMEA\] Treaty* formatter on the Node ID
- files - *\[InforMEA\] Files* formatter on the files field
- keywords - *\[InforMEA\] Entity Reference in all languages* formatter on the term field
- updated - Node "Changed (serializable)" field, using default formatter, with custom date format Y-m-d\TH:i:s\Z
- For any field with no value, you can use the *\[InforMEA\] Null formatter* on the node ID field.

### Decision fields
- status - decision status - usually the *Key* formatter for List (text) fields
- number - decision number - usually *\[InforMEA\] Decision number* formatter to remove the "decision" string from the number
- published - usually *\[InforMEA\] Decision published date* formatter on the date field
- meetingId - add a relation to the meeting entity, display its ID in the same way as the node id.
- meetingTitle - add a relation to the meeting entity, display its title in the same way as the node title.
- meetingUrl - add a relation to the meeting entity, display its URL in the same way as the node url.
- displayOrder

### Meetings fields

- start - default formatter, with custom date format Y-m-d\TH:i:s\Z on the meeting start date
- end - default formatter, with custom date format Y-m-d\TH:i:s\Z on the meeting end date
- abbreviation - Default formatter on the Abbreviation field
- repetition - usually *Key* formatter value for List (text) fields
- kind - usually *Key* formatter value for List (text) fields
- access - either "Custom text" view field with "public", *Key* formatter on a List (text) field
- status - usually *\[InforMEA\] Meeting status* formatter on the node ID field
- imageUrl - usually the Image field formatter to URL only, absolute
- imageCopyright - *\[InforMEA\] Meeting image copyright (from alt field)* on the image field if the copyright text is in the alt field. Otherwise, use the default formatter on a text field
- location
- city
- country - *\[InforMEA\] Display Country as ISO3* formatter on the country entity reference field
- latitude
- longitude
- type - usually *Key* formatter on List fields.

### Contacts

- country - see above
- position
- department
- email
- address
- phoneNumber
- fax
- fullName
- displayOrder
- type - usually *\[InforMEA\] Multiple value List (text)* formatter

### National reports

- files
- submission - Y-m-d formatter on the date field
- country - ISO3 formatter

## Upgrading to the contrib version of the module

1. Run `./path/to/contrib/informea_api/bin/update-old-view.sh path/to/view.informea_api.yml TREATY_NAME`

e.g. `./web/modules/contrib/informea_api/bin/update-old-view.sh config/sync/views.view.informea_api.yml cites`

2. Run `drush cim -y`
3. Check that the InforMEA api view is still working properly.
