<?php

namespace Drupal\informea_api\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\informea_api\SerializedData;
use Drupal\views\Plugin\views\field\EntityField;

/**
 * Display entity field data in a serialized display.
 *
 * @ViewsField("informea_field_export")
 */
class InformeaEntityFieldExport extends EntityFieldExport {

}
