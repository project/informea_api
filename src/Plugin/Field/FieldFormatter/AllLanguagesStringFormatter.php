<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'all_languages_string' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_all_languages_string",
 *   label = @Translation("[InforMEA] String in all languages"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *     "computed",
 *     "computed_string",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class AllLanguagesStringFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use SerializerObjectTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('language_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $languages = $this->languageManager->getLanguages();
    $defaultLanguageId = $this->languageManager->getDefaultLanguage()->getId();

    /** @var \Drupal\Core\Entity\TranslatableInterface $entity */
    $entity = $items->getEntity();

    $values = [];
    if (!$entity->isTranslatable() || !$items->getFieldDefinition()->isTranslatable()) {
      if (!$items->isEmpty()) {
        $values = [$defaultLanguageId => $items->value];
      }
    }

    $fieldName = $this->fieldDefinition->getName();
    foreach ($languages as $languageId => $language) {
      if (!$entity->isTranslatable()) {
        continue;
      }

      if (!$entity->hasTranslation($languageId)) {
        continue;
      }

      $translation = $entity->getTranslation($languageId);
      $value = $translation->get($fieldName)->value;
      if (empty($value)) {
        continue;
      }

      $values[$languageId] = $value;
    }

    return $this->serialize($values);
  }

}
