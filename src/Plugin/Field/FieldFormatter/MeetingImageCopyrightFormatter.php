<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'meeting_image_copyright' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_meeting_image_copyright",
 *   label = @Translation("[InforMEA] Meeting image copyright (from alt field)"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class MeetingImageCopyrightFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getMeetingImageCopyright($items));
  }

  /**
   * Get the image copyright.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return string|null
   *   The copyright in the alt field.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getMeetingImageCopyright(FieldItemList $field) {
    if ($field->isEmpty()) {
      return NULL;
    }

    return !empty($field->first()->getValue()['alt']) ?
      $field->first()->getValue()['alt'] :
      NULL;
  }

}
