<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'visibility' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_visibility",
 *   label = @Translation("[InforMEA] Visibility"),
 *   field_types = {
 *     "string",
 *     "computed",
 *     "computed_string",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class VisibilityFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $access = 'public';
    if (strpos(strtolower($items->value), 'private') !== FALSE) {
      $access = NULL;
    }

    return $this->serialize($access);
  }

}
