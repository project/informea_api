<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'multiple_value_list_text' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_multiple_value_list_text",
 *   label = @Translation("[InforMEA] Multiple value List (text)"),
 *   field_types = {
 *     "integer",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class MultipleValueListTextFormatter extends FormatterBase {

  use SerializerObjectTrait;


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'use_keys' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['use_keys'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use keys instead of values'),
      '#default_value' => $this->getSetting('use_keys'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Use keys: @use_keys', [
      '@use_keys' => $this->getSetting('use_keys')
        ? $this->t('Yes')
        : $this->t('No'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getFieldValues($items));
  }

  /**
   * Get the list text values.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return array
   *   The field values.
   */
  public function getFieldValues(FieldItemList $field) {
    if ($field->isEmpty()) {
      return NULL;
    }
    $values = array_column($field->getValue(), 'value');
    if (empty(!$this->configuration['use_keys'])) {
      return $values;
    }
    $allowed_values = $field->getSetting('allowed_values');
    $value_labels = [];
    foreach ($values as $value) {
      $value_labels[] = $allowed_values[$value];
    }
    return $value_labels;
  }

}
