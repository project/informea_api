<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'informea_power_tags' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_power_tags",
 *   label = @Translation("[InforMEA] Powertagging tags"),
 *   description = @Translation("Display powertagging tags for informea export."),
 *   field_types = {
 *     "powertagging_tags"
 *   }
 * )
 */
class InformeaPowerTagsFormatter extends InformeaTermEntityReferenceFormatter {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getEntities(FieldItemList $field) {
    $value = [];
    foreach ($field as $item) {
      if (empty($item->target_id)) {
        continue;
      }

      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($item->target_id);
      if (empty($term)) {
        continue;
      }

      $value[] = $this->getEntity($term);
    }

    return $value;
  }

}
