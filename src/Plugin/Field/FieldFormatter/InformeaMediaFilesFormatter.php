<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'informea_api_media_files' formatter.
 *
 * Use this plugin instead of informea_api_files when the documents
 * that want to be exposed are media references.
 *
 * @FieldFormatter(
 *   id = "informea_api_media_files",
 *   label = @Translation("[InforMEA] Media Files"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class InformeaMediaFilesFormatter extends InformeaFilesFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) : array {
    if (!$items instanceof EntityReferenceFieldItemListInterface) {
      return [];
    }
    $files = [];
    foreach ($items->referencedEntities() as $media) {
      if (!$media instanceof MediaInterface) {
        continue;
      }
      /** @var \Drupal\media\Plugin\media\Source\File $source */
      $source = $media->getSource();
      $configuration = $source->getConfiguration();
      $files = array_merge($files, $this->getFiles($media->get($configuration['source_field'])));
    }
    return $this->serialize($files);
  }

}
