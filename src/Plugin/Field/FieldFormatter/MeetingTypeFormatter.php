<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'meeting_type' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_meeting_type",
 *   label = @Translation("[InforMEA] Meeting type"),
 *   field_types = {
 *     "integer",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class MeetingTypeFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getMeetingType($items));
  }

  /**
   * Get the meeting type.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return string|null
   *   The meeting type.
   */
  public function getMeetingType(FieldItemList $field) {
    $value = array_column($field->getValue(), 'value');
    $value = reset($value);
    if (!empty($value)) {
      $value = strtolower($value);
    }
    return $value ?: NULL;
  }

}
