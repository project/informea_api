<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'treaty' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_treaty",
 *   label = @Translation("[InforMEA] Treaty"),
 *   field_types = {
 *     "integer",
 *     "string",
 *     "entity_reference",
 *   }
 * )
 */
class TreatyFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'treaties' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['treaties'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Treaties'),
      '#description' => $this->t('Treaty machine names. Separate each treaty with a comma.'),
      '#default_value' => $this->getSetting('treaties'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Treaties: @treaties', ['@treaties' => $this->getSetting('treaties')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $treaties = $this->getTreaties($items);
    return $this->serialize(reset($treaties));
  }

  /**
   * Get the treaties.
   *
   * @return string
   *   The treaties.
   */
  protected function getTreaties(FieldItemListInterface $items) {
    $treaties = $this->getSetting('treaties');
    $treaties = explode(',', $treaties);
    foreach ($treaties as &$treaty) {
      $treaty = trim($treaty);
    }
    return $treaties;
  }

}
