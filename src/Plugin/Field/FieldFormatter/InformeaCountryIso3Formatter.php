<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'informea_country_iso3' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_country_iso3",
 *   label = @Translation("[InforMEA] Display Country as ISO3"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class InformeaCountryIso3Formatter extends EntityReferenceFormatterBase {

  use SerializerObjectTrait;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'iso3_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $entity = $this->fieldDefinition->getTargetEntityTypeId();
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity);
    $options = [
      '' => $this->t('- Select -'),
    ];

    foreach ($bundles as $bundle => $info) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity, $bundle);
      foreach ($field_definitions as $field_definition) {
        if ($field_definition->getType() == 'string') {
          $options[$field_definition->getName()] = $field_definition->getLabel();
        }
      }
    }

    $elements = [];
    $elements['iso3_field'] = [
      '#type' => 'select',
      '#title' => $this->t('ISO3 field'),
      '#options' => $options,
      '#default_value' => $this->getSetting('iso3_field'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getIso3($items, $langcode));
  }

  /**
   * Get the ISO3 values.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field items.
   *
   * @return array
   *   The ISO3 values.
   */
  public function getIso3(FieldItemListInterface $items, $langcode) {
    if (empty($items->getValue())) {
      return [];
    }

    $field = $this->configuration['iso3_field'] ?? 'field_country_iso3';

    $results = [];
    foreach ($items->referencedEntities() as $item) {
      if ($item->hasField($field)) {
        $results[] = $item->get($field)->value;
      }
    }

    return $results;
  }

}
