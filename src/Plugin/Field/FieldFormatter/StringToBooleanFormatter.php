<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'string_to_boolean_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_string_to_boolean",
 *   label = @Translation("String to boolean formatter"),
 *   field_types = {
 *     "string",
 *     "list_string",
 *   }
 * )
 */
class StringToBooleanFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getValue($items));
  }

  /**
   * Get field value to boolean.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return boolean
   *   Yes if the string field value is yes.
   */
  public function getValue(FieldItemList $field) {
    $mapping = [
      'yes' => TRUE,
      'no' => FALSE,
    ];

    $field_value = strtolower($field->value);
    $value = isset($mapping[$field_value]) ? $mapping[$field_value] : NULL;

    return $value;
  }

}
