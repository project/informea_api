<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'meeting_status' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_meeting_status",
 *   label = @Translation("[InforMEA] Meeting status"),
 *   field_types = {
 *     "integer",
 *     "entity_reference",
 *   }
 * )
 */
class MeetingStatusFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * Tentative meeting status.
   */
  const MEETING_STATUS_TENTATIVE = 'tentative';

  /**
   * Confirmed meeting status.
   */
  const MEETING_STATUS_CONFIRMED = 'confirmed';

  /**
   * Postponed meeting status.
   */
  const MEETING_STATUS_POSTPONED = 'postponed';

  /**
   * Cancelled meeting status.
   */
  const MEETING_STATUS_CANCELLED = 'cancelled';

  /**
   * No date meeting status.
   */
  const MEETING_STATUS_NODATE = 'nodate';

  /**
   * Over meeting status.
   */
  const MEETING_STATUS_OVER = 'over';

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManagerInterface $entity_field_manager, DateFormatterInterface $date_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'start_date_field' => '',
        'end_date_field' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $entity = $this->fieldDefinition->getTargetEntityTypeId();
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('node');
    $options = [
      '' => $this->t('- Select -'),
    ];

    foreach ($bundles as $bundle => $info) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity, $bundle);
      foreach ($field_definitions as $field_definition) {
        if ($field_definition->getType() == 'datetime' || $field_definition->getType() == 'daterange') {
          $options[$field_definition->getName()] = $field_definition->getLabel();
        }
      }
    }

    $elements = [];
    $elements['start_date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Start date field'),
      '#options' => $options,
      '#default_value' => $this->getSetting('start_date_field'),
    ];
    $elements['end_date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('End date field'),
      '#options' => $options,
      '#default_value' => $this->getSetting('end_date_field'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $start_date_field = $this->getSetting('start_date_field');
    $end_date_field = $this->getSetting('end_date_field');

    return [
      $this->t('Start date field: @start_date_field', ['@start_date_field' => $start_date_field]),
      $this->t('End date field: @end_date_field', ['@end_date_field' => $end_date_field]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getMeetingStatus($items));
  }

  /**
   * Get a meeting status.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return string|null
   *   The meeting status string.
   */
  public function getMeetingStatus(FieldItemList $field) {
    $parent = $field->getEntity();

    $start_date_fields = [
      $this->getSetting('start_date_field') ?? '',
      'field_sess_start_date',
      'field_event_start_date',
      'field_date_range',
    ];

    $end_date_fields = [
      $this->getSetting('end_date_field') ?? '',
      'field_sess_start_date',
      'field_event_start_date',
      'field_date_range',
    ];

    $start = $this->getDateFieldValue($parent, $start_date_fields, 'start');
    $end = $this->getDateFieldValue($parent, $end_date_fields, 'end');

    if (empty($start) || empty($end)) {
      return static::MEETING_STATUS_NODATE;
    }

    if ($end instanceof DrupalDateTime) {
      $endTime = $end->getTimestamp();
    }
    else {
      $endTime = $end->value;
      $endTime = strtotime($endTime);
    }

    if ($endTime < time()) {
      return static::MEETING_STATUS_OVER;
    }

    return static::MEETING_STATUS_CONFIRMED;
  }

  /**
   * Get the date value of a field.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param array $field_list
   *   The date fields.
   * @param string $type
   *   The date type.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The date value.
   */
  protected function getDateFieldValue(FieldableEntityInterface $entity, array $field_list, $type = 'start') {
    foreach ($field_list as $field) {
      if (empty($field)) {
        continue;
      }

      if (!$entity->hasField($field)) {
        continue;
      }

      $field_type = $entity->get($field)->getFieldDefinition()->getType();
      if (!in_array($field_type, ['daterange', 'datetime'])) {
        continue;
      }

      $value = $entity->get($field);
      if ($field_type == 'daterange') {
        return $type == 'start' ? $value->start_date : $value->end_date;
      }

      return $value;
    }

    return NULL;
  }

}
