<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'meeting_virtual' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_meeting_virtual",
 *   label = @Translation("[InforMEA] Meeting virtual field"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */
class MeetingVirtualFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getMeetingVirtual($items));
  }

  /**
   * Check if a meeting is virtual.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return bool
   *   True if the meeting is virtual.
   */
  public function getMeetingVirtual(FieldItemList $field) {
    return strpos(strtolower($field->value), 'online') !== FALSE;
  }

}
