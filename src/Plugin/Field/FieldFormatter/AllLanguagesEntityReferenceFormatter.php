<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'all_languages_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_all_languages_entity_reference",
 *   label = @Translation("[InforMEA] Entity Reference in all languages"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class AllLanguagesEntityReferenceFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use SerializerObjectTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('language_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $languages = $this->languageManager->getLanguages();
    $defaultLanguageId = $this->languageManager->getDefaultLanguage()->getId();

    /** @var \Drupal\Core\Entity\TranslatableInterface $entity */
    $entity = $items->getEntity();

    $summaries = [];
    if (!$entity->isTranslatable() || !$items->getFieldDefinition()->isTranslatable()) {
      if (!$items->isEmpty()) {
        $summaries = [$defaultLanguageId => $items->summary];
      }
    }

    $fieldName = $this->fieldDefinition->getName();
    foreach ($languages as $languageId => $language) {
      if (!$entity->isTranslatable()) {
        continue;
      }

      if (!$entity->hasTranslation($languageId)) {
        continue;
      }

      $translation = $entity->getTranslation($languageId);
      $referencedEntities = $translation->get($fieldName)->referencedEntities();
      if (empty($entity)) {
        continue;
      }

      foreach ($referencedEntities as $referencedEntity) {
        if ($referencedEntity instanceof TermInterface) {
          $summaries[$languageId] = $referencedEntity->getName();
        }
        if ($referencedEntity instanceof NodeInterface) {
          $summaries[$languageId] = $referencedEntity->getTitle();
        }
      }
    }

    return $this->serialize($summaries);
  }

}
