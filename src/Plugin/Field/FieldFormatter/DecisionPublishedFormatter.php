<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'decision_published' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_decision_published",
 *   label = @Translation("[InforMEA] Decision published date"),
 *   field_types = {
 *     "daterange",
 *     "datetime",
 *     "flexible_daterange",
 *   }
 * )
 */
class DecisionPublishedFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getDate($items));
  }

  /**
   * Get the decision published date.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field.
   *
   * @return string
   *   The date.
   */
  protected function getDate(FieldItemListInterface $items) {
    if ($items->isEmpty()) {
      return NULL;
    }

    if (!empty($items->end_value)) {
      return date('Y-m-d', strtotime($items->end_value));
    }

    if (!empty($items->value)) {
      return date('Y-m-d', strtotime($items->value));
    }

    return NULL;
  }

}
