<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'treaty' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_treaties",
 *   label = @Translation("[InforMEA] Treaties"),
 *   field_types = {
 *     "integer",
 *     "string",
 *     "entity_reference",
 *   }
 * )
 */
class TreatiesFormatter extends TreatyFormatter {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getTreaties($items));
  }

}
