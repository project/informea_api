<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\informea_api\SerializedData;

/**
 * Class used for serializing data.
 */
trait SerializerObjectTrait {

  /**
   * Serialize an object.
   *
   * @param mixed $object
   *   The object.
   *
   * @return array
   *   The serialized data.
   */
  protected function serialize($object) {
    return [
      [
        '#type' => 'data',
        '#data' => SerializedData::create($object),
      ],
    ];
  }

}
