<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\FileInterface;

/**
 * Plugin implementation of the 'informea_files' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_files",
 *   label = @Translation("[InforMEA] Files"),
 *   field_types = {
 *     "file",
 *   }
 * )
 */
class InformeaFilesFormatter extends FormatterBase {

  use SerializerObjectTrait;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FileUrlGeneratorInterface $file_url_generator, LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->fileUrlGenerator = $file_url_generator;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('file_url_generator'), $container->get('language_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return $this->serialize($this->getFiles($items));
  }

  /**
   * Get the files of an entity.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field.
   *
   * @return array
   *   The files array.
   */
  public function getFiles(FieldItemList $field) {
    $parent = $field->getEntity();
    $value = [];
    $languages = $this->languageManager->getLanguages();
    foreach ($languages as $languageId => $language) {
      if (!$parent->hasTranslation($languageId)) {
        continue;
      }

      $translation = $parent->getTranslation($languageId);
      foreach ($translation->get($field->getName()) as $item) {
        if (!$item->entity instanceof FileInterface) {
          continue;
        }

        $file = $this->getFileObjectFromFileEntity($item->entity, $translation);
        if (!empty($value[$file['id'] . $languageId])) {
          continue;
        }
        $value[$file['id'] . $languageId] = $file;
      }
    }

    return array_values($value);
  }

  /**
   * Get the file object from an entity.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   * @param \Drupal\Core\Entity\EntityInterface $parent
   *   The parent entity.
   *
   * @return array
   *   The file info.
   */
  protected function getFileObjectFromFileEntity(File $file, EntityInterface $parent) {
    $url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    return [
      'id' => $file->id(),
      'url' => $url,
      'content' => NULL,
      'mimeType' => pathinfo($url, PATHINFO_EXTENSION),
      'language' => $parent->get('langcode')->value,
      'filename' => $file->label(),
    ];
  }

}
