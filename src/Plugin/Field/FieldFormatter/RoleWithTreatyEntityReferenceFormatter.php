<?php

namespace Drupal\informea_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Role with treaty' formatter.
 *
 * @FieldFormatter(
 *   id = "informea_api_role_with_treaty",
 *   label = @Translation("[InforMEA] Role with treaty"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class RoleWithTreatyEntityReferenceFormatter extends EntityReferenceLabelFormatter {

  use SerializerObjectTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() : array {
    $settings = [
      'treaties' => '',
    ] + parent::defaultSettings();
    $settings['link'] = FALSE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) : array {

    $form['treaties'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Treaty'),
      '#description' => $this->t('Treaty machine name.'),
      '#default_value' => $this->getSetting('treaties'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() : array {
    $summary = [];

    $summary[] = $this->t('Treaties: @treaties', ['@treaties' => $this->getSetting('treaties')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) : array {
    $roles = [];
    $elements = parent::viewElements($items, $langcode);
    $treaties = $this->getTreaties($items);
    foreach ($elements as $element) {
      $roles[] = [
        'treaty' => reset($treaties),
        'role' => $element['#plain_text'],
      ];

    }
    return $this->serialize($roles);
  }

  /**
   * Get the treaties.
   *
   * @return array
   *   The treaties.
   */
  protected function getTreaties(FieldItemListInterface $items) : array {
    $treaties = $this->getSetting('treaties');
    $treaties = explode(',', $treaties);
    foreach ($treaties as &$treaty) {
      $treaty = trim($treaty);
    }
    return $treaties;
  }

}
